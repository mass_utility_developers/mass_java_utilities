package edu.uw.bothell.css.dsl.MASS;

import org.cytoscape.model.CyNetwork;
import org.cytoscape.model.CyNetworkFactory;
import org.cytoscape.model.CyNetworkManager;
import org.cytoscape.model.CyNode;
import org.cytoscape.session.CyNetworkNaming;
import org.cytoscape.view.layout.CyLayoutAlgorithm;
import org.cytoscape.view.layout.CyLayoutAlgorithmManager;
import org.cytoscape.view.model.CyNetworkView;
import org.cytoscape.view.model.CyNetworkViewFactory;
import org.cytoscape.view.model.CyNetworkViewManager;
import org.cytoscape.view.model.View;
import org.cytoscape.work.AbstractTaskFactory;
import org.cytoscape.work.TaskIterator;

import java.util.Set;
import java.util.stream.Collectors;

public class CreateNetworkTaskFactory extends AbstractTaskFactory {
	private final CyNetworkManager netMgr;
	private final CyNetworkFactory cnf;
	private final CyNetworkNaming namingUtil;
	private final CyNetworkViewFactory cnvf;
	private final CyNetworkViewManager cnvm;
	private final CyLayoutAlgorithmManager clam;

	public CreateNetworkTaskFactory(final CyNetworkManager netMgr,
									final CyNetworkNaming namingUtil, final CyNetworkFactory cnf,
									final CyNetworkViewFactory cnvf, final CyNetworkViewManager cnvm,
									final CyLayoutAlgorithmManager clam){
		this.netMgr = netMgr;
		this.namingUtil = namingUtil;
		this.cnf = cnf;
		this.cnvf = cnvf;
		this.cnvm = cnvm;
		this.clam = clam;
	}
	
	public TaskIterator createTaskIterator(){
		TaskIterator iterator = new TaskIterator(new CreateNetworkTask(netMgr, namingUtil, cnf, cnvf, cnvm));

		for (CyNetworkView view : cnvm.getNetworkViewSet()) {
			iterator.append(clam.getDefaultLayout().createTaskIterator(view, clam.getDefaultLayout().createLayoutContext(),
					CyLayoutAlgorithm.ALL_NODE_VIEWS, null));
		}

		return iterator;
	}
}
