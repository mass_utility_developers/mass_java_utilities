/*

 */
package edu.uw.bothell.css.dsl.MASS;

import edu.uw.bothell.css.dsl.MASS.graph.transport.GraphModel;
import edu.uw.bothell.css.dsl.MASS.graph.transport.VertexModel;
import org.cytoscape.model.*;
import org.cytoscape.session.CyNetworkNaming;
import org.cytoscape.view.layout.CyLayoutAlgorithm;
import org.cytoscape.view.layout.CyLayoutAlgorithmManager;
import org.cytoscape.view.model.*;
import org.cytoscape.view.presentation.property.BasicVisualLexicon;
import org.cytoscape.view.presentation.property.EdgeBendVisualProperty;
import org.cytoscape.view.presentation.property.LineTypeVisualProperty;
import org.cytoscape.work.AbstractTask;
import org.cytoscape.work.TaskIterator;
import org.cytoscape.work.TaskMonitor;
import org.cytoscape.view.presentation.property.NodeShapeVisualProperty;

//import edu.uw.bothell.css.dsl.MASS.graph.transport.VertexModel;
//import edu.uw.bothell.css.dsl.MASS.graph.transport.GraphModel;

import java.awt.*;
import java.awt.image.DirectColorModel;
import java.util.*;
import java.util.List;
import java.util.stream.Collectors;

/**
 *
 */
public class CreateNetworkTask extends AbstractTask {
	
	private final CyNetworkManager netMgr;
	private final CyNetworkFactory cnf;
	private final CyNetworkNaming namingUtil;
	private final CyNetworkViewFactory cnvf;
	private final CyNetworkViewManager cnvm;


	public CreateNetworkTask(final CyNetworkManager netMgr, final CyNetworkNaming namingUtil, final CyNetworkFactory cnf,
							 final CyNetworkViewFactory cnvf, final CyNetworkViewManager cnvm){
		this.netMgr = netMgr;
		this.cnf = cnf;
		this.namingUtil = namingUtil;
		this.cnvf = cnvf;
		this.cnvm = cnvm;
	}

	private CyNetwork graphToCyNetwork(GraphModel graph) {
		if (graph == null) return null;

		// Create an empty network
		CyNetwork cyNetwork = cnf.createNetwork();
		cyNetwork.getRow(cyNetwork).set(CyNetwork.NAME,
				namingUtil.getSuggestedNetworkTitle(graph.getName()));

		// add agents column
		List<Integer> agentsDefaultList = Collections.singletonList(0);

		cyNetwork.getDefaultNodeTable().createListColumn("agents", Integer.class, false,
				agentsDefaultList);

			List<VertexModel> vertices = graph.getVertices();

		// TODO: This might need to be a map
		CyNode [] nodes = new CyNode[vertices.size()];

		Map<Object, CyNode> nodesMap = new HashMap<>(nodes.length);

		// Add vertices to the network
		for (int v = 0; v < vertices.size(); v++) {
			VertexModel vertex = vertices.get(v);

			nodes[v] = cyNetwork.addNode();

			nodesMap.put(vertex.id, nodes[v]);

			// set name for new vertex
			cyNetwork.getDefaultNodeTable().getRow(nodes[v].getSUID()).set("name", vertex.id.toString());

			List randomAgents = new ArrayList(10);

			final Random rand = new Random();

			for (int i = 0; i < 10; i++) {
				randomAgents.add(i);
			}

			cyNetwork.getDefaultNodeTable().getRow(nodes[v].getSUID()).set("agents", randomAgents);
		}

		// Add edges after all vertices are created
		for (int v = 0; v < vertices.size(); v++) {
			VertexModel vertex = vertices.get(v);

			for (Object neighbor : vertex.neighbors.stream().filter(n -> !n.equals(vertex.id)).collect(Collectors.toList())) {
				CyNode neighborNode = nodesMap.get(neighbor);

				if (neighborNode == null) {
					// Create new node with different style

					neighborNode = cyNetwork.addNode();

					nodesMap.put(neighbor, neighborNode);
					//throw new RuntimeException("Invalid neighbor");

					// Set nodes name indicating orphaned
					cyNetwork.getDefaultNodeTable().getRow(neighborNode.getSUID()).set("name", "Orphan: " + neighbor);
				}

				CyEdge edge = cyNetwork.addEdge(nodes[v], neighborNode, true);

				CyRow edgeRow = cyNetwork.getDefaultEdgeTable().getRow(edge.getSUID());

				edgeRow.set("name", v + ":" + neighbor);
				edgeRow.set("interaction", vertex.id + " --> " + neighbor);
			}
		}

		return cyNetwork;
	}

	private void createView(CyNetwork network) {
		final Collection<CyNetworkView> views = cnvm.getNetworkViews(network);
		CyNetworkView networkView = null;
		if(views.size() != 0)
			networkView = views.iterator().next();

		if (networkView == null) {
			// create a new view for my network
			networkView = cnvf.createNetworkView(network);

			networkView.setViewDefault(BasicVisualLexicon.NODE_SHAPE, NodeShapeVisualProperty.ROUND_RECTANGLE);

			for (View<CyNode> viewNode : networkView.getNodeViews()) {
				Map<String, Object> nodeValues = networkView.getModel().getRow(viewNode.getModel()).getAllValues();

				if (((String) nodeValues.get("name")).contains("Orphan")) {
					viewNode.setVisualProperty(BasicVisualLexicon.NODE_SHAPE, NodeShapeVisualProperty.ELLIPSE);
					viewNode.setVisualProperty(BasicVisualLexicon.NODE_WIDTH, 10.0);
					viewNode.setVisualProperty(BasicVisualLexicon.NODE_HEIGHT, 10.0);
					viewNode.setVisualProperty(BasicVisualLexicon.NODE_FILL_COLOR, Color.RED);
//					viewNode.setVisualProperty(BasicVisualLexicon.NODE_LABEL,
//							((String) nodeValues.getOrDefault("name", "Orphan " + viewNode.getSUID()))
//									.substring("Orphan ".length()));
				} else {
					viewNode.setVisualProperty(BasicVisualLexicon.NODE_LABEL,
							nodeValues.getOrDefault("name", "Vertex " + viewNode.getSUID()));
					viewNode.setVisualProperty(BasicVisualLexicon.NODE_WIDTH, 100.0);

					viewNode.setVisualProperty(BasicVisualLexicon.NODE_FILL_COLOR, Color.CYAN);
				}
			}

			for (View<CyEdge> viewEdge : networkView.getEdgeViews()) {
				viewEdge.setVisualProperty(BasicVisualLexicon.EDGE_PAINT, LineTypeVisualProperty.SOLID);
			}

			cnvm.addNetworkView(networkView);
		} else {
			System.out.println("networkView already existed.");
		}
	}

	public void run(TaskMonitor monitor) {
		GraphReader reader = new GraphReader("localhost", 8165);

		CyNetwork network = graphToCyNetwork(reader.getCurrentGraph());

		netMgr.addNetwork(network);

		createView(network);

		// Set the variable destroyNetwork to true, the following code will destroy a network
		boolean destroyNetwork = false;
		if (destroyNetwork){
			// Destroy it
			 netMgr.destroyNetwork(network);
		}
	}
}
