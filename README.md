## MASS - Cytoscape Plugins

This repository contains two plugins for [Cytoscape](https://cytoscape.org/). The modules were written for Cytoscape 3
and should work as is for version 3.x

#### Pre-requisites

These modules require a JDK version 1.8 or newer and maven (tested with 3.5.4)

#### Build and Install

##### Linux

```bash
mvn package
ln -s ~cytoscape/export-network/target/export-network-1.0.0-RELEASE.jar ~/Cytoscape/3/apps/installed/
ln -s ~cytoscape/import-network/target/import-network-1.0.0-RELEASE.jar ~/Cytoscape/3/apps/installed/
```