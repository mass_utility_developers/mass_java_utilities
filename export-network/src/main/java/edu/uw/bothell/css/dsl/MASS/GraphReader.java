package edu.uw.bothell.css.dsl.MASS;

//import edu.uw.bothell.css.dsl.MASS.graph.transport.GraphModel;

import edu.uw.bothell.css.dsl.MASS.graph.transport.GraphModel;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;
import java.util.Arrays;
import java.util.Random;

public class GraphReader {
    private final String massHost;
    private final int massPort;

    public GraphReader(String masterHost, int port) {
        this.massHost = masterHost;
        this.massPort = port;
    }



    public GraphModel getCurrentGraph() {
//        GraphModel result = new GraphModel();
//
//        result.setName("MASS Graph");
//
//        generateRandomGraph(result, 100);

        GraphModel result = null;

        try {
            Socket socket = new Socket(massHost, massPort);

            ObjectOutputStream outStream = new ObjectOutputStream(socket.getOutputStream());
            ObjectInputStream inputStream = new ObjectInputStream(socket.getInputStream());

            outStream.writeObject("getGraph");
            result = (GraphModel) inputStream.readObject();
        } catch (IOException | ClassNotFoundException e) {
            e.printStackTrace();
        }

        return result;
    }

    private static void generateRandomGraph(GraphModel result, int vertexCount) {
        if (vertexCount < 1) {
            vertexCount = 1;
        }

        Random rand = new Random(System.currentTimeMillis());

        int neighborMax = vertexCount / 2;

        for (int i = 0; i < vertexCount; i++) {
            int neighborCount = rand.nextInt(neighborMax);

            Integer [] neighbors = new Integer[neighborCount];

            for (int n = 0; n < neighborCount; n++) {
                neighbors[n] = rand.nextInt(vertexCount);
            }

            result.addVertex(i, Arrays.asList(neighbors));
        }
    }
}
