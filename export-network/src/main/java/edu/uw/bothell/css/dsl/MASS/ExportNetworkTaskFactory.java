package edu.uw.bothell.css.dsl.MASS;

import org.cytoscape.application.CyApplicationManager;
import org.cytoscape.model.CyNetworkFactory;
import org.cytoscape.model.CyNetworkManager;
import org.cytoscape.session.CyNetworkNaming;
import org.cytoscape.view.layout.CyLayoutAlgorithm;
import org.cytoscape.view.layout.CyLayoutAlgorithmManager;
import org.cytoscape.view.model.CyNetworkView;
import org.cytoscape.view.model.CyNetworkViewFactory;
import org.cytoscape.view.model.CyNetworkViewManager;
import org.cytoscape.work.AbstractTaskFactory;
import org.cytoscape.work.TaskIterator;

public class ExportNetworkTaskFactory extends AbstractTaskFactory {
	private final CyNetworkManager netMgr;
	private final CyNetworkFactory cnf;
	private final CyNetworkNaming namingUtil;
	private final CyNetworkViewFactory cnvf;
	private final CyNetworkViewManager cnvm;
	private final CyLayoutAlgorithmManager clam;
	private final CyApplicationManager cam;

	public ExportNetworkTaskFactory(final CyNetworkManager netMgr,
									final CyNetworkNaming namingUtil, final CyNetworkFactory cnf,
									final CyNetworkViewFactory cnvf, final CyNetworkViewManager cnvm,
									final CyLayoutAlgorithmManager clam, CyApplicationManager cyApplicationManager){
		this.netMgr = netMgr;
		this.namingUtil = namingUtil;
		this.cnf = cnf;
		this.cnvf = cnvf;
		this.cnvm = cnvm;
		this.clam = clam;
		this.cam = cyApplicationManager;
	}
	
	public TaskIterator createTaskIterator(){
		TaskIterator iterator = new TaskIterator(new ExportNetworkTask(netMgr, namingUtil, cnf, cnvf, cnvm, cam));

		// TODO: I don't think this one was working but really not needed in export
		for (CyNetworkView view : cnvm.getNetworkViewSet()) {
			iterator.append(clam.getDefaultLayout().createTaskIterator(view, clam.getDefaultLayout().createLayoutContext(),
					CyLayoutAlgorithm.ALL_NODE_VIEWS, null));
		}

		return iterator;
	}
}
