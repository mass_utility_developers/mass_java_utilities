package edu.uw.bothell.css.dsl.MASS;

import org.cytoscape.application.CyApplicationManager;
import org.cytoscape.model.CyNetworkFactory;
import org.cytoscape.session.CyNetworkNaming;
import org.cytoscape.model.CyNetworkManager;
import org.cytoscape.view.layout.CyLayoutAlgorithmManager;
import org.cytoscape.view.model.CyNetworkViewFactory;
import org.cytoscape.view.model.CyNetworkViewManager;
import org.cytoscape.work.ServiceProperties;
import org.cytoscape.work.TaskFactory;
import org.osgi.framework.BundleContext;
import org.cytoscape.service.util.AbstractCyActivator;
import java.util.Properties;

import static org.cytoscape.work.ServiceProperties.ENABLE_FOR;


public class CyActivator extends AbstractCyActivator {
	public CyActivator() {
		super();
	}


	public void start(BundleContext bc) {
		CyNetworkManager cyNetworkManagerServiceRef = getService(bc,CyNetworkManager.class);
		CyNetworkNaming cyNetworkNamingServiceRef = getService(bc,CyNetworkNaming.class);
		CyNetworkFactory cyNetworkFactoryServiceRef = getService(bc,CyNetworkFactory.class);
		CyNetworkViewFactory cyNetworkViewFactoryServiceRef = getService(bc,CyNetworkViewFactory.class);
		CyNetworkViewManager cyNetworkViewManagerServiceRef = getService(bc,CyNetworkViewManager.class);
		CyLayoutAlgorithmManager cyLayoutAlgorithmManager = getService(bc, CyLayoutAlgorithmManager.class);
		CyApplicationManager cyApplicationManager = getService(bc, CyApplicationManager.class);

		ExportNetworkTaskFactory exportNetworkTaskFactory =
				new ExportNetworkTaskFactory(cyNetworkManagerServiceRef,cyNetworkNamingServiceRef,
						cyNetworkFactoryServiceRef, cyNetworkViewFactoryServiceRef, cyNetworkViewManagerServiceRef,
						cyLayoutAlgorithmManager, cyApplicationManager);

		Properties sample05TaskFactoryProps = new Properties();
		sample05TaskFactoryProps.setProperty("preferredMenu","Apps.MASS");
		sample05TaskFactoryProps.setProperty("title","Export Network");
		sample05TaskFactoryProps.setProperty(ENABLE_FOR, "network");
		sample05TaskFactoryProps.setProperty(ServiceProperties.NETWORK_APPS_MENU, "true");
		sample05TaskFactoryProps.setProperty(ServiceProperties.IN_NETWORK_PANEL_CONTEXT_MENU, "true");

		registerService(bc, exportNetworkTaskFactory,TaskFactory.class, sample05TaskFactoryProps);
	}
}

